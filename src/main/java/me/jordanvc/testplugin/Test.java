package me.jordanvc.testplugin;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Test extends JavaPlugin{
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]){
		if(cmd.getName().equalsIgnoreCase("test")){
			if(sender instanceof Player){
			  Player p = (Player) sender;
			  World w = p.getWorld();
			  Location loc = p.getLocation();
			  
			  w.spawnEntity(loc, EntityType.SILVERFISH);
			  w.playEffect(loc, Effect.GHAST_SHRIEK, 1);
			  w.playEffect(loc, Effect.SMOKE, 1);
			}
			else{
			  sender.sendMessage(ChatColor.RED + "You must be a player to run this command!");
			}
		}
		return false;
	}
}
